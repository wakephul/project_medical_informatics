import sys
import os
from bcrypt import hashpw, gensalt
from sqlalchemy import create_engine
from sqlalchemy import MetaData
from sqlalchemy import Table, Column
from datetime import datetime
import time as cosodeltempo


engine = create_engine("sqlite:///infomed.db")
metadata = MetaData()

practitioner_table = Table("practitioner", metadata, autoload = True, autoload_with = engine)
user_table = Table("user", metadata, autoload = True, autoload_with = engine)

if len(sys.argv) == 8:
	today = str(datetime.now())[:10]
	todayunix = cosodeltempo.mktime(datetime.strptime(today, "%Y-%m-%d").timetuple())
	engine.execute(practitioner_table.insert().values(fiscalcode = sys.argv[1], hashpsw = hashpw(bytes(sys.argv[2], encoding = 'utf-8'), gensalt()), name = sys.argv[3], surname = sys.argv[4], city = sys.argv[5], country = sys.argv[6], CAP = sys.argv[7]))
	engine.execute(user_table.insert().values(fiscalcode = sys.argv[1], role = 1, registrationdate = todayunix))
else:
	print ("USAGE :\npython3 practitioner.py FISCALCODE PASSWORD NAME SURNAME CITY COUNTRY CAP\nE.g.: python3 practitioner.py 1234567890123456 doctorinthehospital Doctor TheDoctor city country 11111\n\nInsert them in the correct order.")
