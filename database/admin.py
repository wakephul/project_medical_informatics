import sys
import os
from bcrypt import hashpw, gensalt
from sqlalchemy import create_engine
from sqlalchemy import MetaData
from sqlalchemy import Table, Column
from datetime import datetime
import time as cosodeltempo

engine = create_engine("sqlite:///infomed.db")
metadata = MetaData()

admin_table = Table("admin", metadata, autoload = True, autoload_with = engine)
user_table = Table("user", metadata, autoload = True, autoload_with = engine)

if len(sys.argv) == 5:
	today = str(datetime.now())[:10]
	todayunix = cosodeltempo.mktime(datetime.strptime(today, "%Y-%m-%d").timetuple())
	engine.execute(admin_table.insert().values(fiscalcode = sys.argv[1], hashpsw = hashpw(bytes(sys.argv[2], encoding = 'utf-8'), gensalt()), name = sys.argv[3], surname = sys.argv[4]))
	engine.execute(user_table.insert().values(fiscalcode = sys.argv[1], role = 3, registrationdate = todayunix))
else:
	print ("USAGE :\npython3 admin.py FISCALCODE PASSWORD NAME SURNAME\nE.g.: python3 admin.py 1234567890123456 sysadmin Technical Admin\nInsert them in the correct order.")