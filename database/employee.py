import sys
import os
from bcrypt import hashpw, gensalt
from sqlalchemy import create_engine
from sqlalchemy import MetaData
from sqlalchemy import Table, Column
from datetime import datetime
import time as cosodeltempo


engine = create_engine("sqlite:///infomed.db")
metadata = MetaData()

employee_table = Table("employee", metadata, autoload = True, autoload_with = engine)
user_table = Table("user", metadata, autoload = True, autoload_with = engine)

if len(sys.argv) == 9:
	today = str(datetime.now())[:10]
	todayunix = cosodeltempo.mktime(datetime.strptime(today, "%Y-%m-%d").timetuple())
	engine.execute(employee_table.insert().values(fiscalcode = sys.argv[1], hashpsw = hashpw(bytes(sys.argv[2], encoding = 'utf-8'), gensalt()), name = sys.argv[3], surname = sys.argv[4], city = sys.argv[5], country = sys.argv[6], CAP = sys.argv[7], practitioner = sys.argv[8]))
	engine.execute(user_table.insert().values(fiscalcode = sys.argv[1], role = 2, registrationdate = todayunix))
else:
	print ("USAGE :\npython3 employee.py FISCALCODE PASSWORD NAME SURNAME CITY COUNTRY CAP PRACTITIONER\nE.g.: python3 employee.py 1234567890123456 patientinthehospital Patient ThePatient city country 12345 1\n\nInsert them in the correct order.")