import os
from sqlalchemy import create_engine
from sqlalchemy import MetaData
from sqlalchemy import Table, Column
from sqlalchemy import Integer, String, Enum, DateTime, ForeignKey, Text
from sqlalchemy.sql import func

engine = create_engine("sqlite:///infomed.db")

metadata = MetaData()
employee_table = Table("employee", metadata,
				Column("fiscalcode", String(16), primary_key = True),
				Column("name", String(20), nullable = False),
				Column("surname", String(20), nullable = False),
				Column("username", String(40), nullable = True, unique = True),
				Column("hashpsw", String(120), nullable = False),
				Column("birthday", DateTime, nullable = True),
				Column("city", String(16), nullable = False),
				Column("country", String(16), nullable = False),
				Column("CAP", Integer, nullable = False),
				Column("gender", Enum('m', 'f', 'n'), nullable = False, server_default = 'n'),
				Column("birthday", DateTime, nullable = True),
				Column("telephonenumber", String(15), nullable = True),
				Column("email", String(40), nullable = True),
				Column("practitioner", Integer, ForeignKey("practitioner.practitioner_id"), nullable = False)			
				)

practitioner_table = Table("practitioner", metadata,
				Column("fiscalcode", String(16), nullable = False),
				Column("practitioner_id", Integer, primary_key = True, autoincrement = True),
				Column("email", String(40), nullable = True),
				Column("hashpsw", String(120), nullable = False),
				Column("name", String(25), nullable = False),
				Column("surname", String(25), nullable = False),
				Column("telephonenumber", String(15), nullable = True),
				Column("birthday", DateTime, nullable = True),
				Column("city", String(16), nullable = False),
				Column("country", String(16), nullable = False),
				Column("CAP", Integer, nullable = False),
				Column("gender", Enum('m', 'f', 'n'), nullable = False, server_default = 'n'),
				Column("birthday", DateTime, nullable = True),
				Column("telephonenumber", String(15), nullable = True),
				Column("email", String(40), nullable = True)
				)

admin_table = Table("admin", metadata,
				Column("fiscalcode", String(16), primary_key = True),
				Column("name", String(25), nullable = False),
				Column("surname", String(25), nullable = False),
				Column("email", String(40), nullable = True),
				Column("hashpsw", String(120), nullable = False)
				)
user_table = Table("user", metadata,
				Column("counter", Integer, primary_key = True, autoincrement = True),
				Column("role", Integer, nullable = False), #1=practitioner, 2=employee, 3=admin
				Column("fiscalcode", String(16), nullable = False),
				Column("registrationdate", Integer, nullable = False)
				)

employeevisits_table = Table("employeevisits", metadata,
				Column("fiscalcode", String(16), ForeignKey("employee.fiscalcode"), primary_key = True),
				Column("last_visit_id", Integer, ForeignKey("visits.visit_id"), nullable = True), #what's the point in having this?
				Column("notes", Text, nullable = True),
				Column("next_visit_date", Integer, nullable = True),
				Column("insert", String(256), nullable = True), #What is this?
				Column("personal_practitioner", Integer, ForeignKey("practitioner.practitioner_id"))
				)

visits_table = Table("visits", metadata,
				Column("visit_id", Integer, primary_key = True, autoincrement = True),
				Column("place", Text, nullable = False),
				Column("date", Integer, nullable = False),
				Column("practitioner", Integer, ForeignKey("practitioner.practitioner_id")),
				Column("employee", String(16), ForeignKey("employee.fiscalcode")),
				Column("follow_up", Integer, nullable = True, server_default = '0') #WHAT IS THIS
				)

parameters_table = Table("parameters", metadata,
					Column("parameters_id", Integer, primary_key=True, autoincrement=True),
					Column("fiscalcode", String(16), ForeignKey("employee.fiscalcode"), nullable = False),
					Column("heartrate", Integer, server_default = '0'),
					Column("bloodpressuremin", Integer, server_default = '0'),
					Column("bloodpressuremax", Integer, server_default = '0'),
					Column("sleepquality", Integer, server_default = '0'),
					Column("height", Integer, server_default = '0'),
					Column("weight", Integer, server_default = '0'),
					Column("BMI", Integer, server_default = '0'),
					Column("foodcalories", Integer, server_default = '0'),
					Column("hoursofactivity", Integer, server_default = '0'),
					Column("bloodresults", Integer, server_default = '0'),
					Column("polysomnography", Integer, server_default = '0'),
					Column("perceivedstresslevel1", Integer, server_default = '0'),
					Column("perceivedstresslevel2", Integer, server_default = '0'),
					Column("perceivedstresslevel3", Integer, server_default = '0'),
					Column("dateofinsertion", Integer, nullable = False),
					Column("homeorwork", Enum('h', 'w'), nullable = False, server_default = 'h')
				)

thresholds_table = Table("thresholds", metadata,
					Column("fiscalcode", String(16), ForeignKey("employee.fiscalcode"), primary_key = True),
					Column("heartrate", Integer, server_default = '0'),
					Column("bloodpressuremin", Integer, server_default = '0'),
					Column("bloodpressuremax", Integer, server_default = '0'),
					Column("sleepquality", Integer, server_default = '0'),
					Column("height", Integer, server_default = '0'),
					Column("weight", Integer, server_default = '0'),
					Column("BMI", Integer, server_default = '0'),
					Column("foodcalories", Integer, server_default = '0'),
					Column("hoursofactivity", Integer, server_default = '0'),
					Column("bloodresults", Integer, server_default = '0'),
					Column("polysomnography", Integer, server_default = '0'),
					Column("perceivedstresslevel", Integer, server_default = '0')
				)
logins_table = Table("logins", metadata,
					Column("loginscounter", Integer, primary_key = True, autoincrement = True),
					Column("date", Integer, nullable = False),


				)

metadata.create_all(engine)
