import re
import json
from time import sleep
import time as cosodeltempo
import random
from bcrypt import hashpw, checkpw, gensalt
from flask import Flask, request, session, render_template, redirect, url_for, Markup
from flaskrun import flaskrun
from datetime import timedelta, datetime
from sqlalchemy import create_engine, MetaData, Table, Column
from sqlalchemy.sql import select
from operator import itemgetter

from werkzeug import generate_password_hash, check_password_hash #da utilizzare in qualche modo per garantire sicurezza bella


from wtforms import Form, TextField, PasswordField, validators, IntegerField
from wtforms.validators import Email, AnyOf, DataRequired

app = Flask(__name__)

app.secret_key = 'Key=Segreta'
app.config['SECURITY_PASSWORD_SALT'] = 'Bella!Segreta&Salata'
 
# MySQL (sqlite db) configurations
engine = create_engine("sqlite:///database/infomed.db")
metadata = MetaData()
employee_table = Table("employee", metadata, autoload = True, autoload_with = engine)
practitioner_table = Table("practitioner", metadata, autoload = True, autoload_with = engine)
admin_table = Table("admin", metadata, autoload = True, autoload_with = engine)
employeevisits_table = Table("employeevisits", metadata, autoload = True, autoload_with = engine)
visits_table = Table("visits", metadata, autoload = True, autoload_with = engine)
user_table = Table("user", metadata, autoload = True, autoload_with = engine)
parameters_table = Table("parameters", metadata, autoload = True, autoload_with = engine)
logins_table = Table("logins", metadata, autoload = True, autoload_with = engine)

# Create MySQL connection
# conn = mysql.connect()

def test_reg(string, your_pattern):
    pattern = re.compile(your_pattern)
    if not re.match(pattern, string):
        return 0
    return 1

mail_pattern = r"\"?([-a-zA-Z0-9.`?{}]+@\w+\.\w+)\"?"

def user_check(session):
    if not 'fiscalcode' in session:
        return -1
    user = engine.execute(select([user_table.c.role], user_table.c.fiscalcode == session['fiscalcode'])).fetchone()
    if user is None:
        return -1
    if user['role']>0 and user['role']<4:
        return user['role']
    return -1

def check_constraints(content):
    if {'email'} <= content.keys():
        if len(content['email']) > 50 or not test_reg(content['email'], mail_pattern):
            return 0
    if {'name'} <= content.keys():
        if len(content['name']) > 25:
            return 0
        name = content['name'].replace("'", " ").split(' ')
        for i in name:
            if i.isalpha()==False:
                return 0;
    if {'surname'} <= content.keys():
        if len(content['surname']) > 25:
            return 0
        name = content['surname'].replace("'", " ").split(' ')
        for i in name:
            if i.isalpha()==False:
                return 0;
    if {'password'} <= content.keys():
        if len(content['password']) < 8 or len(content['password']) > 64:
            return 0
        if content['password'].find(" ") != -1:
            return 0
    if {'confirm_password'} <= content.keys():
        if len(content['confirm_password']) < 8 or len(content['confirm_password']) > 64:
            return 0
        if content['confirm_password'].find(" ") != -1:
            return 0
    if {'new_password'} <= content.keys():
        if len(content['new_password']) < 8 or len(content['new_password']) > 64:
            return 0
        if content['new_password'].find(" ") != -1:
            return 0
    if {'old_password'} <= content.keys():
        if len(content['old_password']) < 8 or len(content['old_password']) > 64:
            return 0
        if content['old_password'].find(" ") != -1:
            return 0
    if {'fiscalcode'} <= content.keys():
        if len(content['fiscalcode']) != 16:
            return 0
    return 1


class login_form(Form):
    fiscalcode = TextField('fiscalcode', [validators.Required()])
    password = PasswordField('Password', [validators.Required()])

class form_choosepatient(Form):
    fiscalcode = TextField('fiscalcode', [validators.Required()])

class addemployee_form(Form):
    fiscalcode = TextField('fiscalcode', [validators.Required()])
    name = TextField('name', [validators.Required()])
    surname = TextField('surname', [validators.Required()])
    password = PasswordField('password', [validators.Required()])
    city = TextField('city', [validators.Required()])
    country = TextField('country', [validators.Required()])
    CAP = TextField('CAP', [validators.Required()])
    practitioner = TextField('practitioner', [validators.Required()])

class addpractitioner_form(Form):
    fiscalcode = TextField('fiscalcode', [validators.Required()])
    name = TextField('name', [validators.Required()])
    surname = TextField('surname', [validators.Required()])
    password = PasswordField('password', [validators.Required()])
    city = TextField('city', [validators.Required()])
    country = TextField('country', [validators.Required()])
    CAP = TextField('CAP', [validators.Required()])

class modifyemployee_form(Form):
    fiscalcode = TextField('fiscalcode', [validators.Required()])
    name = TextField('name', [validators.Required()])
    surname = TextField('surname', [validators.Required()])
    password = PasswordField('password', [validators.Required()])
    city = TextField('city', [validators.Required()])
    country = TextField('country', [validators.Required()])
    CAP = TextField('CAP', [validators.Required()])
    practitioner = TextField('practitioner', [validators.Required()])

class modifypractitioner_form(Form):
    fiscalcode = TextField('fiscalcode', [validators.Required()])
    name = TextField('name', [validators.Required()])
    surname = TextField('surname', [validators.Required()])
    password = PasswordField('password', [validators.Required()])
    city = TextField('city', [validators.Required()])
    country = TextField('country', [validators.Required()])
    CAP = TextField('CAP', [validators.Required()])

class deleteemployee_form(Form):
    fiscalcode = TextField('fiscalcode', [validators.Required()])

class deletepractitioner_form(Form):
    fiscalcode = TextField('fiscalcode', [validators.Required()])

class inhomeparameters_form(Form):
    fiscalcode = TextField('fiscalcode', [validators.Required()])
    height = TextField('height', [validators.Required()])
    weight = TextField('weight', [validators.Required()])
    BMI = TextField('BMI', [validators.Required()])
    foodcalories = TextField('foodcalories', [validators.Required()])
    hoursofactivity = TextField('hoursofactivity', [validators.Required()])
        


@app.before_request
def make_session_permanent():
    session.permanent = True


@app.route('/login', methods = ["GET", "POST"])
def login():

    if user_check(session) == 1:
        return redirect(url_for("practitionerhome"))

    if user_check(session) == 2:
        return redirect(url_for("employeehome"))

    if user_check(session) == 3:
        return redirect(url_for("adminhome"))

    form = login_form(request.form)

    if request.method == "POST" and form.validate():

        content = {'fiscalcode':str(form.fiscalcode.data), 'password':str(form.password.data)}

        user = engine.execute(select([user_table.c.role], user_table.c.fiscalcode == content['fiscalcode'])).fetchone()

        today = str(datetime.now())[:10]
        todayunix = cosodeltempo.mktime(datetime.strptime(today, "%Y-%m-%d").timetuple())

        if user is None:
            error="User not found"

            return render_template("login.html", error = error)

        elif user[0] == 1:
            practitioner = engine.execute(select([practitioner_table.c.hashpsw, practitioner_table.c.name, practitioner_table.c.surname, practitioner_table.c.practitioner_id], practitioner_table.c.fiscalcode == content['fiscalcode'])).fetchone()
            if practitioner is not None:
                hashpsw, name, surname, practitioner_id = practitioner
            if checkpw(content['password'].encode('utf-8'), hashpsw.encode('utf-8')):
                session['fiscalcode'] = content['fiscalcode']
                session['fullname'] = name.capitalize() + ' ' + surname.capitalize()
                session['practitioner_id'] = practitioner_id
                engine.execute(logins_table.insert().values(date = todayunix))
                return redirect(url_for('practitionerhome'))

        elif user[0] == 2:
            employee = engine.execute(select([employee_table.c.hashpsw, employee_table.c.name, employee_table.c.surname], employee_table.c.fiscalcode == content['fiscalcode'])).fetchone()
            if employee is not None:
                hashpsw, name, surname = employee
                print(content['password'])
                if checkpw(content['password'].encode('utf-8'), hashpsw.encode('utf-8')):
                    session['fiscalcode'] = content['fiscalcode']
                    session['fullname'] = name.capitalize() + ' ' + surname.capitalize()
                    engine.execute(logins_table.insert().values(date = todayunix))
                    return redirect(url_for('employeehome'))

        elif user[0] == 3:
            admin = engine.execute(select([admin_table.c.hashpsw, admin_table.c.name, admin_table.c.surname], admin_table.c.fiscalcode == content['fiscalcode'])).fetchone()
            if admin is not None:
                hashpsw, name, surname = admin
                if checkpw(content['password'].encode('utf-8'), hashpsw.encode('utf-8')):
                    session['fiscalcode'] = content['fiscalcode']
                    session['fullname'] = name.capitalize() + ' ' + surname.capitalize()
                    engine.execute(logins_table.insert().values(date = todayunix))
                    return redirect(url_for('adminhome'))

        error="Something went wrong, try again please"
        return render_template("login.html", error = error)

    return render_template("login.html")


#################.      PRACTITIONER       .#################

@app.route('/practitionerhome', methods = ["GET", "POST"])
def practitionerhome():
    if user_check(session) != 1:
        return redirect(url_for("login"))

    practitioner_id = engine.execute(select([practitioner_table.c.practitioner_id], practitioner_table.c.fiscalcode == session['fiscalcode'])).fetchone()
    practitioner_id = practitioner_id[0]
    employees_list = engine.execute(select([employee_table.c.name, employee_table.c.surname, employee_table.c.fiscalcode], employee_table.c.practitioner == practitioner_id))
    employees_list = employees_list.fetchall()

    to_send = []
    for employee in employees_list:

        name, surname, fiscalcode = employee
        lastvisit = engine.execute(select([employeevisits_table.c.last_visit_id], employeevisits_table.c.fiscalcode == fiscalcode)).fetchone()
        try:
            if lastvisit != None:
                print(lastvisit)
                print(lastvisit[0])
                lastvisit = lastvisit[0]
                print(lastvisit)
                humanlastvisit = datetime.fromtimestamp(int(lastvisit)).strftime('%d-%m-%Y')
                employeedictionary = dict(name = name, surname = surname, fiscalcode = fiscalcode, lastvisit = humanlastvisit)
                to_send.append(employeedictionary)
            else:
                employeedictionary = dict(name = name, surname = surname, fiscalcode = fiscalcode, lastvisit = None)
                to_send.append(employeedictionary)
        except:
            pass
    to_send = sorted(to_send, key=itemgetter('surname'))
    return render_template("practitionerhome.html", employees_list = to_send)


@app.route('/visitsoftheday', methods = ["GET", "POST"])
def visitsoftheday():
    if user_check(session) != 1:
        return redirect(url_for("login"))
    
    visits_list = []
    practitioner_id = engine.execute(select([practitioner_table.c.practitioner_id], practitioner_table.c.fiscalcode == session['fiscalcode'])).fetchone()
    practitioner_id = practitioner_id[0]
    today = str(datetime.now())[:10]
    todayunix = cosodeltempo.mktime(datetime.strptime(today, "%Y-%m-%d").timetuple())
    dayvisits = engine.execute(select([employeevisits_table.c.fiscalcode], employeevisits_table.c.personal_practitioner == practitioner_id)).fetchall()
    print("day:",dayvisits)
    for employee in dayvisits:
        fiscalcode = employee[0]
        print(fiscalcode)
        next_visit_date = engine.execute(select([employeevisits_table.c.next_visit_date], employeevisits_table.c.fiscalcode == fiscalcode)).fetchone()
        if todayunix == next_visit_date[0]:
            dayperson = engine.execute(select([employee_table.c.name, employee_table.c.surname], employee_table.c.fiscalcode == fiscalcode)).fetchone()
            name = dayperson[0]
            surname = dayperson[1]
            visitdictionary = dict(name = name, surname = surname)
            visits_list.append(visitdictionary)

    return render_template("visitsoftheday.html", visits_list = visits_list)


@app.route('/bookavisit', methods = ["GET", "POST"])
def bookavisit():
    if user_check(session) != 1:
        return redirect(url_for("login"))
    success = ""

    form = form_choosepatient(request.form)

    if request.method == "POST":
        try:
            content = {'fiscalcode':str(form.fiscalcode.data)}
            selected = content['fiscalcode']
            date = request.form['date']
            print(selected)
            dateunix = cosodeltempo.mktime(datetime.strptime(date, "%Y-%m-%d").timetuple())
            print(dateunix)
            practitioner_id = engine.execute(select([practitioner_table.c.practitioner_id], practitioner_table.c.fiscalcode == session['fiscalcode'])).fetchone()
            practitioner_id = practitioner_id[0]
            print(practitioner_id)
            responsible = engine.execute(select([employee_table.c.practitioner], employee_table.c.fiscalcode == selected)).fetchone()
            responsible = responsible[0]
            if responsible == practitioner_id:
                existing = engine.execute(select([employeevisits_table.c.fiscalcode], employeevisits_table.c.fiscalcode == selected)).fetchone()
                print(existing)
                if existing is None:
                    engine.execute(employeevisits_table.insert().values(fiscalcode = selected, personal_practitioner = practitioner_id, next_visit_date = dateunix))
                else:
                    engine.execute(employeevisits_table.update().values(next_visit_date = dateunix).where(employeevisits_table.c.fiscalcode == selected))
                success = "You have successfully scheduled a visit!"
                return render_template("bookavisit.html", success = success)
            else:
                success = "An error occurred while trying to schedule the visit"
                return render_template("bookavisit.html", success = success)
        except:
            success = "An error occurred while trying to schedule the visit"
            return render_template("bookavisit.html", success = success)

    return render_template("bookavisit.html", success = success)


@app.route('/atworkparameters', methods = ["GET", "POST"])
def atworkparameters():
    if user_check(session) != 1:
        return redirect(url_for("login"))

    success = ""

    try:

        form = atworkparameters_form(request.form)

        if request.method == "POST" and form.validate():

            fiscalcode = str(form.fiscalcode.data)
            heartrate = random.randint(50,120)
            bloodpressuremin = str(form.bloodpressuremin.data)
            bloodpressuremax = str(form.bloodpressuremax.data)
            height = str(form.height.data)
            weight = str(form.weight.data)
            BMI = str(form.BMI.data)
            bloodresults = str(form.bloodresults.data)
            polysomnography = str(form.polysomnography.data)
            perceived1 = str(form.perceived1.data)
            perceived2 = str(form.perceived2.data)
            perceived3 = str(form.perceived3.data)
            today = str(datetime.now())[:10]
            todayunix = cosodeltempo.mktime(datetime.strptime(today, "%Y-%m-%d").timetuple())
            engine.execute(parameters_table.insert().values(fiscalcode = fiscalcode, heartrate = heartrate, bloodpressuremin = bloodpressuremin, bloodpressuremax = bloodpressuremax, bloodresults = bloodresults, height = height, weight = weight, BMI = BMI, foodcalories = foodcalories, hoursofactivity = hoursofactivity, dateofinsertion = todayunix, homeorwork = 'w'))
            success = "Successfully added parameters"
            return render_template("atworkparameters.html", success = success)

    except:
        pass

    return render_template("atworkparameters.html", success = success)



@app.route('/practitionerstatistics', methods = ["GET", "POST"])
def practitionerstatistics():
    if user_check(session) != 1:
        return redirect(url_for("login"))
    # temporal trend of all inserted parameters in a given time frame for specific purposes
    # for comparison purposes, across the entire list of employees
    # count how many times a parameter was labeled as abnormal
    return render_template("practitionerstatistics.html", parameters = to_send)

    # ADD PRACTITIONER SET_THRESHOLDS

#################.      EMPLOYEE       .#################

@app.route('/employeehome', methods = ["GET", "POST"])
def employeehome():
    if user_check(session) != 2:
        return redirect(url_for("login"))
    #profile and booked visits
    bookedvisit = engine.execute(select([employeevisits_table.c.next_visit_date], employeevisits_table.c.fiscalcode == session['fiscalcode'])).fetchone()
    if bookedvisit is not None:
        bookedvisit = bookedvisit[0]
        bookedvisit = datetime.fromtimestamp(int(bookedvisit)).strftime("%Y-%m-%d")
    else:
        bookedvisit = "None"
    welcome = "Welcome back, " + ' ' + session['fullname']
    return render_template("employeehome.html", welcome = welcome, bookedvisit = bookedvisit)

@app.route('/inhomeparameters', methods = ["GET", "POST"])
def inhomeparameters():
    if user_check(session) != 2:
        return redirect(url_for("login"))

    success = ""

    try:

        form = inhomeparameters_form(request.form)

        if request.method == "POST" and form.validate():

            fiscalcode = str(form.fiscalcode.data)
            heartrate = random.randint(50,120)
            bloodpressuremin = random.randint(60,120)
            bloodpressuremax = random.randint(90,170)
            sleepquality = random.randint(1,10)
            height = str(form.height.data)
            weight = str(form.weight.data)
            BMI = str(form.BMI.data)
            foodcalories = str(form.foodcalories.data)
            hoursofactivity = str(form.hoursofactivity.data)
            today = str(datetime.now())[:10]
            todayunix = cosodeltempo.mktime(datetime.strptime(today, "%Y-%m-%d").timetuple())
            engine.execute(parameters_table.insert().values(fiscalcode = fiscalcode, heartrate = heartrate, bloodpressuremin = bloodpressuremin, bloodpressuremax = bloodpressuremax, sleepquality = sleepquality, height = height, weight = weight, BMI = BMI, foodcalories = foodcalories, hoursofactivity = hoursofactivity, dateofinsertion = todayunix))
            success = "Successfully added parameters"
            return render_template("inhomeparameters.html", success = success)

    except:
        pass

    return render_template("inhomeparameters.html", success = success)

@app.route('/employeestatistics', methods = ["GET", "POST"])
def employeestatistics():
    if user_check(session) != 2:
        return redirect(url_for("login"))
    # temporal trend of all inserted parameters in a given time frame for specific purposes
    return render_template("employeestatistics.html", parameters = to_send)

#################.      TECHNICAL ADMINISTRATOR       .#################

@app.route('/adminhome', methods = ["GET", "POST"])
def adminhome():
    if user_check(session) != 3:
        return redirect(url_for("login"))

    return render_template("adminhome.html")

@app.route('/addemployee', methods = ["GET", "POST"])
def addemployee():
    if user_check(session) != 3:
        return redirect(url_for("login"))
    success = ""

    try:

        form = addemployee_form(request.form)

        if request.method == "POST" and form.validate():

            fiscalcode = str(form.fiscalcode.data)
            name = str(form.name.data)
            surname = str(form.surname.data)
            password = str(form.password.data)
            city = str(form.city.data)
            country = str(form.country.data)
            CAP = str(form.CAP.data)
            practitioner = str(form.practitioner.data)
            today = str(datetime.now())[:10]
            todayunix = cosodeltempo.mktime(datetime.strptime(today, "%Y-%m-%d").timetuple())

            if engine.execute(select([user_table.c.role], user_table.c.fiscalcode == fiscalcode)).fetchone() is None and fiscalcode is not None:    
                engine.execute(employee_table.insert().values(fiscalcode = fiscalcode, hashpsw = hashpw(bytes(password, encoding = 'utf-8'), gensalt()), name = name, surname = surname, city = city, country = country, CAP = CAP, practitioner = practitioner))
                engine.execute(user_table.insert().values(fiscalcode = fiscalcode, role = 2, registrationdate = todayunix))
                success = "Successfully added" + ' ' + name.capitalize() + ' ' + surname.capitalize()
                return render_template("addemployee.html", success = success)

    except:
        pass

    return render_template("addemployee.html", success = success)

@app.route('/addpractitioner', methods = ["GET", "POST"])
def addpractitioner():
    if user_check(session) != 3:
        return redirect(url_for("login"))
    success = ""

    try:

        form = addpractitioner_form(request.form)

        if request.method == "POST" and form.validate():

            fiscalcode = str(form.fiscalcode.data)
            name = str(form.name.data)
            surname = str(form.surname.data)
            password = str(form.password.data)
            city = str(form.city.data)
            country = str(form.country.data)
            CAP = str(form.CAP.data)
            today = str(datetime.now())[:10]
            todayunix = cosodeltempo.mktime(datetime.strptime(today, "%Y-%m-%d").timetuple())

            if engine.execute(select([user_table.c.role], user_table.c.fiscalcode == fiscalcode)).fetchone() is None and fiscalcode is not None:    
                engine.execute(practitioner_table.insert().values(fiscalcode = fiscalcode, hashpsw = hashpw(bytes(password, encoding = 'utf-8'), gensalt()), name = name, surname = surname, city = city, country = country, CAP = CAP))
                engine.execute(user_table.insert().values(fiscalcode = fiscalcode, role = 1, registrationdate = todayunix))
                success = "Successfully added" + ' ' + name.capitalize() + ' ' + surname.capitalize()
                return render_template("addpractitioner.html", success = success)

    except:
        pass

    return render_template("addpractitioner.html", success = success)

@app.route('/modifyemployee', methods = ["GET", "POST"])
def modifyemployee():
    if user_check(session) != 3:
        return redirect(url_for("login"))
    success = ""
    try:
        form = modifyemployee_form(request.form)

        if request.method == "POST" and form.validate():

            fiscalcode = str(form.fiscalcode.data)
            name = str(form.name.data)
            surname = str(form.surname.data)
            password = str(form.password.data)
            city = str(form.city.data)
            country = str(form.country.data)
            CAP = str(form.CAP.data)
            practitioner = str(form.practitioner.data)

            if engine.execute(select([user_table.c.role], user_table.c.fiscalcode == fiscalcode)).fetchone() is None and fiscalcode is not None:
                success = "User does not exist" 
                return render_template("modifyemployee.html", success = success)
            elif engine.execute(select([user_table.c.role], user_table.c.fiscalcode == fiscalcode)).fetchone() is not None and fiscalcode is not None:
                engine.execute(employee_table.update().values(hashpsw = hashpw(bytes(password, encoding = 'utf-8'), gensalt()), name = name, surname = surname, city = city, country = country, CAP = CAP, practitioner = practitioner).where(employee_table.c.fiscalcode == fiscalcode))
                success = "Successfully modified" + ' ' + name.capitalize() + ' ' + surname.capitalize()
                return render_template("modifyemployee.html", success = success)

    except:
        pass

    return render_template("modifyemployee.html", success = success)

@app.route('/modifypractitioner', methods = ["GET", "POST"])
def modifypractitioner():
    if user_check(session) != 3:
        return redirect(url_for("login"))
    success = ""
    try:
        form = modifypractitioner_form(request.form)

        if request.method == "POST" and form.validate():

            fiscalcode = str(form.fiscalcode.data)
            name = str(form.name.data)
            surname = str(form.surname.data)
            password = str(form.password.data)
            city = str(form.city.data)
            country = str(form.country.data)
            CAP = str(form.CAP.data)

            if engine.execute(select([user_table.c.role], user_table.c.fiscalcode == fiscalcode)).fetchone() is None and fiscalcode is not None:
                success = "User does not exist" 
                return render_template("modifypractitioner.html", success = success)
            elif engine.execute(select([user_table.c.role], user_table.c.fiscalcode == fiscalcode)).fetchone() is not None and fiscalcode is not None:
                engine.execute(practitioner_table.update().values(hashpsw = hashpw(bytes(password, encoding = 'utf-8'), gensalt()), name = name, surname = surname, city = city, country = country, CAP = CAP).where(practitioner_table.c.fiscalcode == fiscalcode))
                success = "Successfully modified" + ' ' + name.capitalize() + ' ' + surname.capitalize()
                return render_template("modifypractitioner.html", success = success)

    except:
        pass

    return render_template("modifypractitioner.html", success = success)

@app.route('/deleteemployee', methods = ["GET", "POST"])
def deleteemployee():
    if user_check(session) != 3:
        return redirect(url_for("login"))
    success = ""

    try:
        form = deleteemployee_form(request.form)

        if request.method == "POST" and form.validate():

            fiscalcode = str(form.fiscalcode.data)

            if engine.execute(select([user_table.c.role], user_table.c.fiscalcode == fiscalcode)).fetchone() is None and fiscalcode is not None:
                success = "User does not exist" 
                return render_template("deleteemployee.html", success = success)
            elif engine.execute(select([user_table.c.role], user_table.c.fiscalcode == fiscalcode)).fetchone() is not None and fiscalcode is not None:
                engine.execute(employee_table.delete().where(employee_table.c.fiscalcode == fiscalcode))
                engine.execute(user_table.delete().where(user_table.c.fiscalcode == fiscalcode))
                success = "Successfully deleted user" + ' ' + fiscalcode
                return render_template("deleteemployee.html", success = success)
    except:
        pass

    return render_template("deleteemployee.html", success = success)


@app.route('/deletepractitioner', methods = ["GET", "POST"])
def deletepractitioner():
    if user_check(session) != 3:
        return redirect(url_for("login"))
    success = ""

    try:
        form = deletepractitioner_form(request.form)

        if request.method == "POST" and form.validate():

            fiscalcode = str(form.fiscalcode.data)

            if engine.execute(select([user_table.c.role], user_table.c.fiscalcode == fiscalcode)).fetchone() is None and fiscalcode is not None:
                success = "User does not exist" 
                return render_template("deletepractitioner.html", success = success)
            elif engine.execute(select([user_table.c.role], user_table.c.fiscalcode == fiscalcode)).fetchone() is not None and fiscalcode is not None:
                engine.execute(practitioner_table.delete().where(practitioner_table.c.fiscalcode == fiscalcode))
                engine.execute(user_table.delete().where(user_table.c.fiscalcode == fiscalcode))
                success = "Successfully deleted user" + ' ' + fiscalcode
                return render_template("deletepractitioner.html", success = success)
    except:
        pass

    return render_template("deletepractitioner.html", success = success)



@app.route('/adminstatistics', methods = ["GET", "POST"])
def adminstatistics():
    if user_check(session) != 3:
        return redirect(url_for("login"))

    labels = []
    values = []
    # statistics regarding number of users and number of logins per day
    registrationlist = engine.execute(select([user_table.c.registrationdate])).fetchall()
    registrationlist.sort()
    print(registrationlist)
    loginlist = engine.execute(select([logins_table.c.date])).fetchall()
    loginlist.sort()
    print("logins", loginlist)
    legendusers = "Registered Users"

    counter = []
    e = 86400

    for i in range(len(registrationlist)):
        if i == 0:
            counter.append(1)
            labels.append(registrationlist[0][0])
        else:
            if registrationlist[i][0] == registrationlist[i-1][0]:
                    counter[-1] += 1
                    labels.append(registrationlist[i][0])
            else:
                counter.append(1)
                labels.append(registrationlist[i][0])

    t_start = labels[0]
    t_end = labels[-1]
    newstart = t_start
    labelsnew = []
    counternew = []
    cont = 1
    contino = 1
    labelsnew.append(labels[0])
    counternew.append(counter[0])

    while(newstart < t_end):

        if labels[cont] == labels[cont-1]:
            cont += 1
        else:
            newstart = newstart + e
            if labels[cont] == newstart:
                labelsnew.append(labels[cont])
                counternew.append(counter[contino])
                cont += 1
                contino += 1
            else:
                labelsnew.append(0)
                counternew.append(0)
                cont += 1

    newdate = []
    for datuzza in labelsnew:
        newdate.append(datetime.fromtimestamp(int(datuzza)).strftime('%Y-%m-%d'))

    newdateusers = newdate
    counternewusers = counternew

    newdatelogins = []
    counternewlogins = []
    legendlogins = "Number of Logins"
    labels = []
    values = []
    counter = []

    for i in range(len(loginlist)):
        if i != 0:
            if loginlist[i][0] == loginlist[i-1][0]:
                if i == 1:
                    counter.append(1)
                    labels.append(loginlist[i][0])
                else:
                    counter[-1] += 1
                    labels.append(loginlist[i][0])
            else:
                counter.append(1)
                labels.append(loginlist[i][0])

    t_start = labels[0]
    t_end = labels[-1]
    newstart = t_start
    labelsnew = []
    counternew = []
    cont = 1
    contino = 1
    labelsnew.append(labels[0])
    counternew.append(counter[0])
    while(newstart < t_end):
        if labels[cont] == labels[cont-1]:
            cont += 1
        else:
            newstart = newstart + e
            if labels[cont] == newstart:
                labelsnew.append(labels[cont])
                counternew.append(counter[contino])
                cont += 1
                contino += 1
            else:
                labelsnew.append(newstart)
                counternew.append(0)

    newdate = []
    for datuzza in labelsnew:
        newdatelogins.append(datetime.fromtimestamp(int(datuzza)).strftime('%Y-%m-%d'))

    counternewlogins = counternew

    return render_template("adminstatistics.html", labelsusers = newdateusers, valuesusers = counternewusers, legendusers = legendusers, labelslogins = newdatelogins, valueslogins = counternewlogins, legendlogins = legendlogins)






@app.route('/logout', methods = ["GET"])
def logout():
    if 'fiscalcode' in session:
        session.pop('fiscalcode', None)
        session.pop('fullname', None)
    return redirect(url_for("login"))

@app.route('/', methods = ["GET", "POST"])
def index():
    return redirect(url_for("login"))

if __name__ == '__main__':
    flaskrun(app)