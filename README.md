## Welcome!
Hi, I am Riccardo Cavadini, and I am more than happy to welcome you to this repo, about a project I've been working on together with my team (Jessica, Letizia, Matteo and Yunus) for a Medical Informatics class at Politecnico di Milano.

This project is about measuring the level of hypertension at work.

This is just a naive version of what we are planning to do, and therefore the graphics is still a work-in-progress and so are some functionalities.

We are also planning to make it all much more minimal than it is right now, meaning that we have redundant information in many web pages and so on, and we plan to remove them asap.

Anyways, please let me show you around!

-----------------

## Setup the environment

Before we start, notice that you should be using python3 tu run this project. From now on, pip will mean pip3 and python will mean python3.

First, you should create a python virtual environment in which you can then install all needed packages (so that you don't have to install them all globally, if you don't need them outside this project).

Once you got into the environment, install all packages (pip install -r requirements.txt).

-----------------

## Make the webapp start

python web.py --debug

The --debug flag is pretty useful if you plan to make changes on the web server while it is running, so that they will show up at runtime.

By default, it will be run on localhost 127.0.0.1 on port 5000.

You can change the parameters by writing:

python web.py --host=Your_Local_IP --port=Your_Favorite_Port

For any question, please email me at: wakephul AT gmail DOT com